import java.awt.*;

/**
 * Created by omfg on 07.11.2016.
 */
public class Sprite {
    private Image image;


    public Sprite(Image image){
        this.image=image;

    }
    public int getWidth(){
        return image.getWidth(null);

    }
    public int getHight(){
        return image.getHeight(null);
    }
    public void draw(Graphics g,int x,int y){
        g.drawImage(image,x,y,null);
    }

}